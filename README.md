# Prueba de Carga con JMeter

Se sube el archivo .jmx en el que se realizó una prueba de carga con la herramienta JMeter en la pagina de desmostración Blaze Demo, también el archivo de ejemplo datos.txt de los datos que podemos agregar en nuestras pruebas.

Los pasos para abrir el Plan de Pruebas:

1. Abrir JMeter.
2. Seleccionar Archivo y luego Abrir.
3. Seleccionar el archivo .jmx.

Los pasos para agregar los datos ejemplo:

1. Seleccionar Configuración del CSV Data Set.
2. En Nombre de Archivo seleecionar Navegar.
3. Seleccionar el documento de texto datos.txt.

Las variables se encuentran establecidas y configuradas en el apartados de Datos en la prueba.

Para este Plan de Pruebas se realizó una selección de origen y destino para un vuelo de ejemplo, se selecciona un vuelo de la lista, se agregan datos de manera dinamica y se continua a la pantalla final.

Se utilizaron dos temporizadores y 3 aserciones, así como 2 receptores para las pruebas.
